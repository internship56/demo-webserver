#!/usr/bin/env python3
"""Track hostnames and store them into a MariaDB databgase"""
# pylint: disable=R0913, C0103

import argparse
import datetime
import os
import socket

from http.server import HTTPServer, BaseHTTPRequestHandler
from sqlalchemy import (
    Column,
    MetaData,
    Table,
    String,
    create_engine,
    desc,
    insert,
    select,
)


class DB:
    """Wrapper over a RDBMS"""

    def __init__(self, server, port, user, password, database, dialect="mysql+pymysql"):
        """Constructor"""

        self._engine = create_engine(
            "%s://%s:%s@%s:%d/%s"
            % (
                dialect,
                user,
                password,
                server,
                port,
                database,
            )
        )
        self._db = None

        # Define table(s)
        self._metadata = MetaData()
        self._access_log = Table(
            "access_log",
            self._metadata,
            Column("hostname", String(128), nullable=False),
            Column("access_time", String(128), nullable=False),
        )

    def init(self):
        """Establish DB connection and create tables"""
        self._db = self._engine.connect()
        self._metadata.create_all(self._engine)

    def insert_record(self):
        "SELECT * FROM access_log"
        query = insert(
            self._access_log,
            (
                socket.gethostname(),
                datetime.datetime.now().strftime("%Y-%m-%d-%H:%M:%S"),
            ),
        )
        return self._db.execute(query)

    def get_records(self, limit=30):
        """SELECT * FROM access_log LIMIT limit ORDER BY access_time DESC"""
        query = select(columns="*", from_obj=self._access_log, limit=limit).order_by(
            desc(self._access_log.c.access_time)
        )
        result_proxy = self._db.execute(query)
        return result_proxy.fetchall()

    def close(self):
        """Close the connection"""
        self._db.close()


class CustomHandler(BaseHTTPRequestHandler):
    """BaseHTTPServer handler extended"""

    def __init__(self, *args, **kwargs):
        """Initialize DB connection"""
        self._db = DB(
            self.cfg.db_host,
            self.cfg.db_port,
            self.cfg.db_user,
            self.cfg.db_password,
            self.cfg.db_name,
        )
        super(CustomHandler, self).__init__(*args, **kwargs)

    def do_GET(self):
        """Handle GET requests"""

        # Establish database connection
        self._db.init()

        # Log the request
        self._db.insert_record()

        # Build the response body
        chunks = ["<h3>ACCESS LOG</h3><hr><br>"]
        for row in self._db.get_records():
            chunks.extend([" - ".join(row), "<br>"])
        message = "".join(chunks)

        # Close the SQL connection
        self._db.close()

        # Send the HTTP response
        self.send_response(200)
        self.send_header("Content-Type", "text/html")
        self.send_header("Content-Length", len(message))
        self.end_headers()
        self.wfile.write(bytes(message, "UTF-8"))


def main():
    """Entrypoint. Parse command line arguments, initialize DB and start the HTTP Server"""

    cli_parser = argparse.ArgumentParser(description="Deployment Server")

    cli_parser.add_argument(
        "--host",
        type=str,
        help="Bind address. Default: 0.0.0.0",
        default=os.environ.get("BIND_ADDRESS", "0.0.0.0"),
        required=False,
    )

    cli_parser.add_argument(
        "--port",
        type=int,
        help="Bind port. Default: 8080",
        default=os.environ.get("BIND_PORT", "8080"),
        required=False,
    )

    cli_parser.add_argument(
        "--db_host",
        type=str,
        help="Database server hostname. Default: localhost",
        default=os.environ.get("DB_HOST", "localhost"),
        required=False,
    )

    cli_parser.add_argument(
        "--db_port",
        type=int,
        help="Database server port. Default: 3306",
        default=os.environ.get("DB_PORT", "3306"),
        required=False,
    )

    cli_parser.add_argument(
        "--db_user",
        type=str,
        help="Database user. Default: sandbox",
        default=os.environ.get("DB_USER", "sandbox"),
        required=False,
    )

    cli_parser.add_argument(
        "--db_password",
        type=str,
        help="Database user password. Default: sandbox",
        default=os.environ.get("DB_PASSWORD", "sandbox"),
        required=False,
    )

    cli_parser.add_argument(
        "--db_name",
        type=str,
        help="Database name. Default: sandbox",
        default=os.environ.get("DB_NAME", "sandbox"),
        required=False,
    )

    cfg = cli_parser.parse_args()

    CustomHandler.cfg = cfg

    http_server = HTTPServer(
        server_address=(cfg.host, cfg.port), RequestHandlerClass=CustomHandler
    )
    try:
        http_server.serve_forever()
    except KeyboardInterrupt:
        print("KeyboardInterrupt received. Closing server.")
        http_server.socket.close()


if __name__ == "__main__":
    main()
