# python-sqlalchemy

Introduction to SQLAlchemy and RDBMS.

This script starts a webserver and a database connection. Each time a GET request is made,
an entry is added to a database table (```access_log```), and as an HTTP response, you
will receive the 30 last access points in the form of ```SERVER HOSTNAME - ACCESS TIME```

### CLI Options

- ``host``. Default: **0.0.0.0**

    Bind address of the webserver. 

- ``port``. Default: **8080**

    Bind port.

- ``db_host``. Default: **localhost**

    Database host to connect to.

- ``db_port``. Default: **3306**

    Database server port.

- ``db_user``. Default: **sandbox**

    Database user.

- ``db_password``. Default: **sandbox**

    Database password.

- ``db_name``. Default: **sandbox**

    Database name.
